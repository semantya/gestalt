#!/usr/bin/env python

import os, sys

sys.path.append(os.path.join(os.path.dirname(__file__), 'lib'))

curr_ns = 'gestalt'

if os.environ['UCHIKOMA_CHIP'].lower().strip() in ['satellite','knowledge']:
    curr_ns = os.environ['UCHIKOMA_CHIP']

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "uchikoma.%s.settings" % curr_ns)

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)

